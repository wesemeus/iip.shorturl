﻿using IIP.Dto.Producers;
using IIP.Dto.Requests.CreateShortUrl;
using MassTransit;

namespace IIP.ShortURL.API.RabbitMq.Producers.CreateShortUrl;

public sealed class CreateShortUrlProducer: ICreateShortUrlProducer
{
    private readonly IRequestClient<CreateShortUrlRequest> _bus;

    public CreateShortUrlProducer(IRequestClient<CreateShortUrlRequest> bus)
    {
        _bus = bus;
    }

    public async Task<CreateShortUrlResponse> RequestAsync(CreateShortUrlRequest request)
    {
        var result = await _bus.GetResponse<CreateShortUrlResponse>(request);
        return result.Message;
    }
}