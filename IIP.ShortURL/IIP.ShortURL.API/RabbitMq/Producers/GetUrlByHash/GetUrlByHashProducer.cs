﻿using IIP.Dto.Producers;
using IIP.Dto.Requests.GetUrlByHash;
using MassTransit;

namespace IIP.ShortURL.API.RabbitMq.Producers.GetUrlByHash;

public sealed class GetUrlByHashProducer: IGetUrlByHashProducer
{
    private readonly IRequestClient<GetUrlByHashRequest> _bus;

    public GetUrlByHashProducer(IRequestClient<GetUrlByHashRequest> bus)
    {
        _bus = bus;
    }

    public async Task<GetUrlByHashResponse> RequestAsync(GetUrlByHashRequest request)
    {
        var result = await _bus.GetResponse<GetUrlByHashResponse>(request);
        return result.Message;
    }
}