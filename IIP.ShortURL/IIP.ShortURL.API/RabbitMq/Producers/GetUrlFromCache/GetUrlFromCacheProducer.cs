﻿using IIP.Dto.Producers;
using IIP.Dto.Requests.GetUrlFromCache;
using MassTransit;

namespace IIP.ShortURL.API.RabbitMq.Producers.GetUrlFromCache;

public sealed class GetUrlFromCacheProducer: IGetUrlFromCacheProducer
{
    private readonly IRequestClient<GetUrlFromCacheRequest> _bus;

    public GetUrlFromCacheProducer(IRequestClient<GetUrlFromCacheRequest> bus)
    {
        _bus = bus;
    }

    public async Task<GetUrlFromCacheResponse> RequestAsync(GetUrlFromCacheRequest request)
    {
        var result = await _bus.GetResponse<GetUrlFromCacheResponse>(request);
        return result.Message;
    }
}