using IIP.Dto.Producers;
using IIP.Logic.Services;
using IIP.Logic.Services.Interfaces;
using IIP.ShortURL.API.RabbitMq.Producers.CreateShortUrl;
using IIP.ShortURL.API.RabbitMq.Producers.GetUrlByHash;
using IIP.ShortURL.API.RabbitMq.Producers.GetUrlFromCache;
using MassTransit;
using Prometheus;

namespace IIP.ShortURL.API;

public static class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddControllers();
        builder.Services.AddSwaggerGen();
        builder.Services.AddScoped<IShortUrlService, ShortUrlService>();
        builder.Services.AddScoped<ICreateShortUrlProducer, CreateShortUrlProducer>();
        builder.Services.AddScoped<IGetUrlByHashProducer, GetUrlByHashProducer>();
        builder.Services.AddScoped<IGetUrlFromCacheProducer, GetUrlFromCacheProducer>();
        
        builder.Services.AddMassTransit(cfg =>
        {
            cfg.SetKebabCaseEndpointNameFormatter();
            cfg.UsingRabbitMq((ctx, c) =>
            {
                c.Host("localhost", "/", h => {
                    h.Username("guest");
                    h.Password("guest");
                    h.UseCluster(cluster =>
                    {
                        cluster.Node("localhost:5672");
                        cluster.Node("localhost:5673");
                    });
                });
                
                c.ConfigureEndpoints(ctx);
            });
        });

        var app = builder.Build();

        app.UseSwagger();
        app.UseSwaggerUI(options =>
        {
            options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
            options.RoutePrefix = string.Empty;
        });
        
        app.UseRouting();
        app.UseHttpMetrics();
        app.MapMetrics();
        app.MapControllers();

        app.Run();
    }
}