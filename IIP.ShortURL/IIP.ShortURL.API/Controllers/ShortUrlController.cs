﻿using IIP.Dto.Producers;
using IIP.Logic.Models;
using IIP.Logic.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace IIP.ShortURL.API.Controllers;

[ApiController]
[Route("/api/v1/[controller]")]
public sealed class ShortUrlController : ControllerBase
{
    private readonly IGetUrlFromCacheProducer _getUrlFromCacheProducer;
    private readonly IShortUrlService _shortUrlService;

    public ShortUrlController(IGetUrlFromCacheProducer getUrlFromCacheProducer,
        IShortUrlService shortUrlService)
    {
        _getUrlFromCacheProducer = getUrlFromCacheProducer;
        _shortUrlService = shortUrlService;
    }

    [HttpPost("short")]
    public async Task<ActionResult> CreateShortUrl([FromBody]ShortUrlRequest request)
    {
        var result =
            await _shortUrlService.CreateShortUrl(HttpContext.Request.Scheme, HttpContext.Request.Host.ToString(), request.Url);
        return Ok(result);
    }
    
    [HttpGet("/{hash}")]
    public async Task RedirectToUrl(string hash)
    {
        var result = await _shortUrlService.GetUrlByHash(hash);
        HttpContext.Response.Redirect(result, true);
    }
}