﻿namespace IIP.Logic.Models;

public sealed record ShortUrlRequest
{
    public string Url { get; set; }
}