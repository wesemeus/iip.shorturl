﻿using IIP.Dto.Producers;
using IIP.Dto.Requests.CreateShortUrl;
using IIP.Dto.Requests.GetUrlFromCache;
using IIP.Logic.Services.Interfaces;

namespace IIP.Logic.Services;

public sealed class ShortUrlService : IShortUrlService
{
    private readonly ICreateShortUrlProducer _createShortUrlProducer;
    private readonly IGetUrlFromCacheProducer _getUrlFromCacheProducer;

    public ShortUrlService(ICreateShortUrlProducer createShortUrlProducer,
        IGetUrlFromCacheProducer getUrlFromCacheProducer)
    {
        _createShortUrlProducer = createShortUrlProducer;
        _getUrlFromCacheProducer = getUrlFromCacheProducer;
    }

    public async Task<string> CreateShortUrl(string scheme, string host, string url)
    {
        var result = await _createShortUrlProducer.RequestAsync(new CreateShortUrlRequest()
        {
            Url = url
        });
        return $"{scheme}://{host}/{result.ShortUrl}";
    }

    public async Task<string> GetUrlByHash(string hash)
    {
        var result = await _getUrlFromCacheProducer.RequestAsync(new GetUrlFromCacheRequest()
        {
            Hash = hash
        });
        return result.Url!;
    }
}