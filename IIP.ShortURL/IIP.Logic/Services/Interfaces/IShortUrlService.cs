﻿namespace IIP.Logic.Services.Interfaces;

public interface IShortUrlService
{
    Task<string> CreateShortUrl(string scheme, string host, string url);

    Task<string> GetUrlByHash(string hash);
}